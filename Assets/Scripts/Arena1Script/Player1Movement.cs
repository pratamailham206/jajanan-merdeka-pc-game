﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SocialPlatforms.Impl;

public class Player1Movement : MonoBehaviour
{
    private NavMeshAgent agent;
    private float agentOriginalSpeed;

    //PLAYER MOVE
    [SerializeField] private float speed = 5;
    [SerializeField] private float desiredRotationSpeed = 0.1f;
    [SerializeField] private float allowPlayerRotation = 0.1f;

    private Vector3 desiredMoveDirection;

    private float inputX;
    private float inputZ;

    private Camera cam;

    public bool player1 = true;
    public bool player2 = false;


    public Transform pickingObjectPos;
    public Collider[] pickingObjectCol;

    public bool canMove;
    private bool isPickingBom;
    public bool stunned;
    public GameObject stunEffect;
    public Vector3 bashDirection;

    public float playerScore;
    void Start()
    {
        playerScore = 00f;
        canMove = true;
        stunned = false;
        cam = Camera.main;
        isPickingBom = false;
        agent = GetComponent<NavMeshAgent>();
        agentOriginalSpeed = agent.speed;
    }

    void Update()
    {       
        DetectingInput();

        if (stunned)
            StunMovement();
    }

    void StunMovement()
    {     
        Vector3 newPosition = transform.position + bashDirection * Time.deltaTime * 2.5f;
        NavMeshHit navhit;
        bool isValid = NavMesh.SamplePosition(newPosition, out navhit, .3f, NavMesh.AllAreas);

        if (!isValid)
            return;

        transform.position = navhit.position;
    }

    public void SetStunned(Vector3 dir)
    {
        bashDirection = dir;        
        StartCoroutine(SetTargetStunned(1.5f));

        IEnumerator SetTargetStunned(float time)
        {
            GetComponent<Player1Movement>().stunned = true;
            Instantiate(stunEffect, new Vector3(transform.position.x, transform.position.y + 1.5f, transform.position.z), transform.rotation);
            GetComponent<Player1Movement>().canMove = false;
            agent.speed = 0;
            yield return new WaitForSeconds(time);
            agent.speed = agentOriginalSpeed;
            GetComponent<Player1Movement>().stunned = false;
            GetComponent<Player1Movement>().canMove = true;
        }
    }

    void PickingBom()
    {
        pickingObjectCol = Physics.OverlapSphere(pickingObjectPos.position, 1.5f);
        foreach (Collider nearbyBom in pickingObjectCol)
        {
            if (nearbyBom.tag == "Bom")
            {
                if(isPickingBom)
                {
                    nearbyBom.gameObject.GetComponent<BomScript>().isBomPicked = false;
                    isPickingBom = false;
                }
                else if(!isPickingBom)
                {
                    nearbyBom.gameObject.GetComponent<BomScript>().playerP = pickingObjectPos;
                    nearbyBom.gameObject.GetComponent<BomScript>().playerPos = gameObject;
                    nearbyBom.gameObject.GetComponent<BomScript>().isBomPicked = true;
                    isPickingBom = true;
                }                               
            }

            else if(nearbyBom.tag == "Heal")
            {
                if (isPickingBom)
                {
                    nearbyBom.gameObject.GetComponent<HealScript>().isHealPicked = false;
                    isPickingBom = false;
                }
                else if (!isPickingBom)
                {
                    nearbyBom.gameObject.GetComponent<HealScript>().playerP = pickingObjectPos;
                    nearbyBom.gameObject.GetComponent<HealScript>().playerPos = gameObject;
                    nearbyBom.gameObject.GetComponent<HealScript>().isHealPicked = true;
                    isPickingBom = true;
                }
            }
        }
    }

    void DetectingInput()
    {
        if (player1 && canMove)
        {
            inputX = Input.GetAxis("Horizontal");
            inputZ = Input.GetAxis("Vertical");

            if (Input.GetKeyDown(KeyCode.Space))
            {
                PickingBom();
            }

            float inputMagnitute = new Vector2(inputX, inputZ).sqrMagnitude;

            if (inputMagnitute > allowPlayerRotation)
            {
                PlayerMoveCharacter();
            }
        }

        if (player2 && canMove)
        {
            inputX = Input.GetAxis("arrowH");
            inputZ = Input.GetAxis("arrowV");

            if (Input.GetKeyDown(KeyCode.P))
            {
                PickingBom();
            }

            float inputMagnitute = new Vector2(inputX, inputZ).sqrMagnitude;

            if (inputMagnitute > allowPlayerRotation)
            {
                PlayerMoveCharacter();
            }
        }               
    }

    void PlayerMoveCharacter()
    {
        if(player1)
        {
            inputX = Input.GetAxis("Horizontal");
            inputZ = Input.GetAxis("Vertical");
        }

        if(player2)
        {
            inputX = Input.GetAxis("arrowH");
            inputZ = Input.GetAxis("arrowV");
        }

        Vector3 axis = new Vector3(inputX, 0, inputZ);

        var forward = cam.transform.forward;
        var right = cam.transform.right;

        forward.y = 0f;
        right.y = 0f;

        forward.Normalize();
        right.Normalize();

        desiredMoveDirection = forward * inputZ + right * inputX;

        if (axis.magnitude < .01f)
            return;

        Vector3 newPosition = transform.position + desiredMoveDirection * Time.deltaTime * speed;
        NavMeshHit hit;
        bool isValid = NavMesh.SamplePosition(newPosition, out hit, .3f, NavMesh.AllAreas);

        if (!isValid)
            return;

        if ((transform.position - hit.position).magnitude >= .02f)
            transform.position = hit.position;

        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(desiredMoveDirection), desiredRotationSpeed);
    }

    public void addScore(float scoreAdded)
    {
        playerScore += scoreAdded;
    }

    public void decreaseScore(float decreasedScore)
    {
        playerScore -= decreasedScore;
    }
}
