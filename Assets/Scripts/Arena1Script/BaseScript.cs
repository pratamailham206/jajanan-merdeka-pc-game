﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseScript : MonoBehaviour
{
    // Start is called before the first frame update
    public float health;
    public GameObject explosionEffect;

    void Start()
    {
        health = 100;
        GetComponent<Arena1Manager>();
    }

    // Update is called once per frame
    void Update()
    {
        if(health == 0)
        {
            BaseDestroyed();
        }
    }

    public void baseDamaged(float Damage)
    {
        health -= Damage;
    }

    public void BaseDestroyed()
    {
        gameObject.SetActive(false);
        Instantiate(explosionEffect, transform.position, transform.rotation);
    }

    public void HealBase(float heal)
    {
        health += heal;
    }
}
