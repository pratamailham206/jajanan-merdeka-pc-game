﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Arena1Manager : MonoBehaviour
{
    public GameObject bom;
    public GameObject heal;

    public float xMinPos;
    public float zMinPos;
    public float xMaxPos;
    public float zMaxPos;

    private float randX;
    private float randZ;

    public static int bomSpawned;
    private Vector3 itemSpawnedin;

    private float startBomSpawn;
    private float durBomSpawn;

    public static int healSpawned;

    private float startHealSpawn;
    private float durHealSpawn;

    public GameObject[] bases;

    public bool base1Destroyed;
    public bool base2Destroyed;
    public bool base3Destroyed;
    public bool base4Destroyed;

    int winner;

    public Image base1Health;
    public Image base2Health;

    public GameObject Player1;
    public GameObject Player2;

    public Text player1Score;
    public Text player2Score;

    void Start()
    {
        base1Destroyed = false;
        base2Destroyed = false;
        base3Destroyed = false;
        base4Destroyed = false;

        startBomSpawn = 0;
        durBomSpawn = 1.5f;

        startHealSpawn = 7;
        durHealSpawn = 4f;

        winner = 0;
                
    }

    // Update is called once per frame
    void Update()
    {
        //BOM SPAWN DURATION
        if(startBomSpawn <= 0)
        {
            if (bomSpawned < 10)
            {
                BomSpawn();
                startBomSpawn = durBomSpawn;
            }
        }
        
        else if(startBomSpawn > 0)
        {
            startBomSpawn -= 1 * Time.deltaTime;
        }

        //HEAL SPAWN DURATION
        if (startHealSpawn <= 0)
        {
            if (healSpawned < 3)
            {
                HealSpawn();
                startHealSpawn = durHealSpawn;
            }
        }

        else if (startHealSpawn > 0)
        {
            startHealSpawn -= 1 * Time.deltaTime;
        }

        //CHECK GAME STATUS
        checkBaseDestroyed();

        playerStatus();

        switch(winner)
        {
            case 1:
                Debug.Log("Player 1 Win");
                break;
            case 2:
                Debug.Log("Player 2 Win");
                break;
            case 3:
                Debug.Log("Player 3 Win");
                break;
            case 4:
                Debug.Log("Player 4 Win");
                break;
        }
    }

    void playerStatus()
    {
        player1Score.GetComponent<Text>().text = Player1.GetComponent<Player1Movement>().playerScore.ToString();
        player2Score.GetComponent<Text>().text = Player2.GetComponent<Player1Movement>().playerScore.ToString();

        base1Health.fillAmount = bases[0].GetComponent<BaseScript>().health / 100;
        base2Health.fillAmount = bases[1].GetComponent<BaseScript>().health / 100;
    }

    void checkBaseDestroyed()
    {
        if (bases[0].GetComponent<BaseScript>().health <= 0)
        {
            base1Destroyed = true;
        }

        if (bases[1].GetComponent<BaseScript>().health <= 0)
        {
            base2Destroyed = true;
        }

        if (bases[2].GetComponent<BaseScript>().health <= 0)
        {
            base3Destroyed = true;
        }

        if (bases[3].GetComponent<BaseScript>().health <= 0)
        {
            base4Destroyed = true;
        }

        if (base2Destroyed && base3Destroyed && base4Destroyed)
        {
            winner = 1;
        }

        if (base1Destroyed && base3Destroyed && base4Destroyed)
        {
            winner = 2;
        }

        if (base2Destroyed && base1Destroyed && base4Destroyed)
        {
            winner = 3;
        }

        if (base2Destroyed && base3Destroyed && base1Destroyed)
        {
            winner = 4;
        }
    }

    public void BomSpawn()
    {
        randX = Random.Range(xMinPos, xMaxPos);
        randZ = Random.Range(zMinPos, zMaxPos);

        itemSpawnedin = new Vector3(randX, 9f, randZ);

        bomSpawned++;
        Instantiate(bom, itemSpawnedin, Quaternion.identity);
    }

    public void HealSpawn()
    {
        randX = Random.Range(xMinPos, xMaxPos);
        randZ = Random.Range(zMinPos, zMaxPos);

        itemSpawnedin = new Vector3(randX, 9f, randZ);

        healSpawned++;
        Instantiate(heal, itemSpawnedin, Quaternion.identity);
    }
}
