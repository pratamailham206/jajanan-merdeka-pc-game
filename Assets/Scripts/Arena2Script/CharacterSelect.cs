﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CharacterSelect : MonoBehaviour
{
    private int skin1SelectedId = 0;
    private int skin2SelectedId = 0;

    public Renderer characterRenderer1;
    public Renderer characterRenderer2;

    public Button StartGameBtn;

    private void Start()
    {
        characterRenderer1.material.SetTexture("_BaseMap", CharacterState.instance.skinTextures[CharacterState.instance.chara1skinId]);
        characterRenderer2.material.SetTexture("_BaseMap", CharacterState.instance.skinTextures[CharacterState.instance.chara2skinId]);
        StartGameBtn.onClick.AddListener(StartGame);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.D))
        {
            ChangeSkin(true, 1);
        }
        if (Input.GetKeyDown(KeyCode.A))
        {
            ChangeSkin(false, 1);
        }
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            ChangeSkin(true, 2);
        }
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            ChangeSkin(false, 2);
        }
    }

    public void StartGame()
    {
        SceneManager.LoadScene("Arena2");
        MusicManager.instance.buttonSFX.Play();
        MusicManager.instance.Change();
    }

    void ChangeSkin(bool _right, int _player)
    {
        MusicManager.instance.tikSFX.Play();
        if (_right && _player == 1)
        {
            if(skin1SelectedId < CharacterState.instance.skinTextures.Length - 1)
            {
                skin1SelectedId++;
            }
            else
            {
                skin1SelectedId = 0;
            }
            characterRenderer1.material.SetTexture("_BaseMap", CharacterState.instance.skinTextures[skin1SelectedId]);
            CharacterState.instance.chara1skinId = skin1SelectedId;
        }
        if (_right && _player == 2)
        {
            if (skin2SelectedId < CharacterState.instance.skinTextures.Length - 1)
            {
                skin2SelectedId++;
            }
            else
            {
                skin2SelectedId = 0;
            }
            characterRenderer2.material.SetTexture("_BaseMap", CharacterState.instance.skinTextures[skin2SelectedId]);
            CharacterState.instance.chara2skinId = skin2SelectedId;
        }
        if (!_right && _player == 1)
        {
            if (skin1SelectedId > 0)
            {
                skin1SelectedId--;
            }
            else
            {
                skin1SelectedId = CharacterState.instance.skinTextures.Length - 1;
            }
            characterRenderer1.material.SetTexture("_BaseMap", CharacterState.instance.skinTextures[skin1SelectedId]);
            CharacterState.instance.chara1skinId = skin1SelectedId;
        }
        if (!_right && _player == 2)
        {
            if (skin2SelectedId > 0 )
            {
                skin2SelectedId--;
            }
            else
            {
                skin2SelectedId = CharacterState.instance.skinTextures.Length - 1;
            }
            characterRenderer2.material.SetTexture("_BaseMap", CharacterState.instance.skinTextures[skin2SelectedId]);
            CharacterState.instance.chara2skinId = skin2SelectedId;
        }
    }
}
